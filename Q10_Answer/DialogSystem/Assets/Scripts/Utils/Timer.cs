﻿using System;
using UnityEngine;

public class Timer
{
    private bool _isRunning;
    
    public float Elapsed { get; private set; }
    public float Duration { get; private set; }

    public event Action OnTimerTick;
    public event Action OnTimerFinished;

    public void Play(float seconds)
    {
        Duration = seconds;
        Elapsed = 0f;
        _isRunning = true;
    }

    public void Tick()
    {
        if (!_isRunning) return;
        
        Elapsed += Time.deltaTime;
        OnTimerTick?.Invoke();

        if (Elapsed < Duration) return;
        
        _isRunning = false;
        OnTimerFinished?.Invoke();
    }
}
