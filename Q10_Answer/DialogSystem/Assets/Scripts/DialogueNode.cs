using System;

public enum EventType
{
    SHOW_BACKGROUND,
    SHOW_CHARACTER = 1,
    SHOW_TEXTBOX = 2,
    HIDE_TEXTBOX = 3,
    PLAY_MUSIC = 4,
    STOP_MUSIC = 5,
    PLAY_SFX = 6,
    WAIT = 7
}

public class DialogueNode
{
    private EventType _eventId;
    private string[] _args;

    public event Action<string> OnShowBackgroundRequested;
    public event Action<string> OnShowCharacterRequested;
    public event Action<string, string> OnShowTextBoxRequested;
    public event Action OnHideTextBoxRequested;
    public event Action<string> OnPlayMusicRequested;
    public event Action OnStopMusicRequested;
    public event Action<string> OnPlaySfxRequested;
    public event Action<string> OnWaitRequested;


    public DialogueNode(string eventId, string[] args)
    {
        _eventId = (EventType) Enum.Parse(typeof(EventType), eventId);
        _args = args;
    }

    public void RegisterListener(IDialogNodeListener listener)
    {
        switch (_eventId)
        {
            case EventType.SHOW_BACKGROUND:
                OnShowBackgroundRequested += listener.ShowBackground;
                return;
            case EventType.SHOW_CHARACTER:
                OnShowCharacterRequested += listener.ShowCharacter;
                return;
            case EventType.SHOW_TEXTBOX:
                OnShowTextBoxRequested += listener.ShowTextBox;
                return;
            case EventType.HIDE_TEXTBOX:
                OnHideTextBoxRequested += listener.HideTextBox;
                return;
            case EventType.PLAY_MUSIC:
                OnPlayMusicRequested += listener.PlayMusic;
                return;
            case EventType.STOP_MUSIC:
                OnStopMusicRequested += listener.StopMusic;
                return;
            case EventType.PLAY_SFX:
                OnPlaySfxRequested += listener.PlaySfx;
                return;
            case EventType.WAIT:
                OnWaitRequested += listener.Wait;
                return;
        }
    }

    public void Read()
    {
        switch (_eventId)
        {
            case EventType.SHOW_BACKGROUND:
                OnShowBackgroundRequested?.Invoke(_args[0]);
                return;
            case EventType.SHOW_CHARACTER:
                OnShowCharacterRequested?.Invoke(_args[0]);
                return;
            case EventType.SHOW_TEXTBOX:
                OnShowTextBoxRequested?.Invoke(_args[0], _args[1]);
                return;
            case EventType.HIDE_TEXTBOX:
                OnHideTextBoxRequested?.Invoke();
                return;
            case EventType.PLAY_MUSIC:
                OnPlayMusicRequested?.Invoke(_args[0]);
                return;
            case EventType.STOP_MUSIC:
                OnStopMusicRequested?.Invoke();
                return;
            case EventType.PLAY_SFX:
                OnPlaySfxRequested?.Invoke(_args[0]);
                return;
            case EventType.WAIT:
                OnWaitRequested?.Invoke(_args[0]);
                return;
        }
    }
}
