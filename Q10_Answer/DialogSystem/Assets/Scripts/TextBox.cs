using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TextBox : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textContent;

    private Image _borderImage;

    private void Awake()
    {
        _borderImage = GetComponent<Image>();
    }

    public void SetContent(Color borderColor, string text)
    {
        _borderImage.color = borderColor;
        textContent.text = text;
    }
}
