using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogBuilder : MonoBehaviour, IDialogNodeListener
{
    [Header("Scene Objects References")]
    [SerializeField] private Image background;
    [SerializeField] private Image character;
    [SerializeField] private TextBox textBox;

    [Header("Dialog File")]
    [SerializeField] private string jsonFileName = "JSON_File_1";

    private int _currentIndex;
    private bool _isPlaying;
    private AudioSource _audioSource;
    private DialogueNode[] _nodes;

    private readonly Timer _timer = new Timer();
    private readonly Dictionary<string, Sprite> _sprites = new Dictionary<string, Sprite>();
    private readonly Dictionary<string, AudioClip> _sounds = new Dictionary<string, AudioClip>();

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        var defaultImg = Resources.Load<Sprite>("Images/Default");
        _sprites.Add("Default", defaultImg);

        var textAsset = Resources.Load<TextAsset>($"Files/{jsonFileName}");
        ParseJSONFile(textAsset.text);
    }

    private void Update()
    {
        _timer.Tick();
    }

    public void ParseJSONFile(string jsonText)
    {
        JSONObject content = JSON.Parse(jsonText) as JSONObject;
        var dialog = content["Dialog"].AsArray;
        var linesCount = dialog.Count;
        _nodes = new DialogueNode[linesCount];
        for (var i = 0; i < linesCount; i++)
        {
            var eventId = dialog[i]["event_id"].ToString().Replace("\"", "");
            var args = ConvertToStringArray(dialog[i]["args"].AsArray);
            _nodes[i] = new DialogueNode(eventId, args);
            _nodes[i].RegisterListener(this);
        }
    }

    public string[] ConvertToStringArray(JSONArray array)
    {
        var newArray = new string[array.Count];

        if (newArray.Length == 0) return newArray;
        
        for (var i = 0; i < array.Count; i++)
        {
            newArray[i] = array[i].ToString().Replace("\"", "");
        }

        return newArray;
    }

    public void StartDialog()
    {
        if (_isPlaying) return;

        _isPlaying = true;
        _currentIndex = -1;
        ReadNode();
    }

    public void ReadNode()
    {
        _currentIndex++;
        if (_currentIndex >= _nodes.Length)
        {
            Debug.Log("Finish Dialog");
            _isPlaying = false;
            return;
        }
        _nodes[_currentIndex].Read();
    }

    public void HideTextBox()
    {
        textBox.gameObject.SetActive(false);
        ReadNode();
    }

    public void PlayMusic(string musicName)
    {
        AudioClip clip;
        if (_sounds.ContainsKey(musicName))
            clip = _sounds[musicName];
        else
            clip = Resources.Load<AudioClip>($"Sounds/Background/{musicName}");
        
        _audioSource.clip = clip;
        _audioSource.loop = true;
        _audioSource.Play();

        ReadNode();
    }

    public void PlaySfx(string sfxName)
    {
        AudioClip clip;
        if (_sounds.ContainsKey(sfxName))
            clip = _sounds[sfxName];
        else
            clip = Resources.Load<AudioClip>($"Sounds/SFX/{sfxName}");
        
        _audioSource.PlayOneShot(clip);

        ReadNode();
    }

    public void ShowBackground(string backgroundName)
    {
        Sprite sprite;
        if (_sprites.ContainsKey(backgroundName))
            sprite = _sprites[backgroundName];
        else
            sprite = Resources.Load<Sprite>($"Images/Backgrounds/{backgroundName}");

        if (!sprite)
            sprite = _sprites["Default"];
        
        background.sprite = sprite;

        ReadNode();
    }

    public void ShowCharacter(string characterName)
    {
        Sprite sprite;
        if (_sprites.ContainsKey(characterName))
            sprite = _sprites[characterName];
        else
            sprite = Resources.Load<Sprite>($"Images/Characters/{characterName}");

        if (!sprite)
            sprite = _sprites["Default"];
        
        character.sprite = sprite;

        ReadNode();
    }

    public void ShowTextBox(string colorName, string text)
    {
        ColorUtility.TryParseHtmlString(colorName, out var color);
        textBox.gameObject.SetActive(true);
        textBox.SetContent(color, text);

        ReadNode();
    }

    public void StopMusic()
    {
        _audioSource.Stop();
        ReadNode();
    }

    public void Wait(string seconds)
    {
        var time = float.Parse(seconds);
        _timer.Play(time);
    }

    private void OnEnable() => _timer.OnTimerFinished += ReadNode;
    private void OnDisable() => _timer.OnTimerFinished -= ReadNode;
}
