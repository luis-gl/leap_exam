public interface IDialogNodeListener
{
    public void ShowBackground(string backgroundName);
    public void ShowCharacter(string characterName);
    public void ShowTextBox(string colorName, string text);
    public void HideTextBox();
    public void PlayMusic(string musicName);
    public void StopMusic();
    public void PlaySfx(string sfxName);
    public void Wait(string seconds);
}