﻿using UnityEngine;

public class ChangeSpriteColor : StateMachineBehaviour
{
    [SerializeField] private Color color = Color.blue;

    private Color _baseColor;
    private SpriteRenderer _renderer;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (!_renderer)
            _renderer = animator.GetComponent<SpriteRenderer>();
        
        _baseColor = _renderer.color;
        _renderer.color = color;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        _renderer.color = _baseColor;
    }
}
