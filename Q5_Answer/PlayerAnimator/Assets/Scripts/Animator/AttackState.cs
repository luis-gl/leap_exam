﻿using UnityEngine;

public enum AttackType
{
    Melee,
    Range
}

public class AttackState : StateMachineBehaviour
{
    [SerializeField] private AttackType attackType = AttackType.Melee;
    [SerializeField] private bool attackTypeToggleValue = false;

    private PlayerController _controller;

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (!_controller)
            _controller = animator.GetComponent<PlayerController>();
        
        switch (attackType)
        {
            case AttackType.Melee:
                _controller.IsMeleeAttacking = attackTypeToggleValue;
                break;
            case AttackType.Range:
                _controller.IsRangeAttacking = attackTypeToggleValue;
                break;
        }
    }
}
