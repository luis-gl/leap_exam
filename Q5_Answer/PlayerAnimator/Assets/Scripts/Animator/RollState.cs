﻿using UnityEngine;

public class RollState : StateMachineBehaviour
{
    [SerializeField] private bool rollToggleValue = false;

    private PlayerController _controller;

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (!_controller)
            _controller = animator.GetComponent<PlayerController>();
        
        _controller.IsRolling = rollToggleValue;
    }
}
