﻿using UnityEngine;

public static class GizmosUtils
{
    public static void DrawBoxCast(Vector2 origin, Vector2 size, Vector2 direction, float distance)
    {
        var width = size.x * 0.5f;
        var height = size.y * 0.5f;

        var offsetPosition = origin + direction * distance;
        var topLeft = new Vector2(-width, height) + offsetPosition;
        var topRight = new Vector2(width, height) + offsetPosition;
        var bottomRight = new Vector2(width, -height) + offsetPosition;
        var bottomLeft = new Vector2(-width, -height) + offsetPosition;
        
        Gizmos.DrawLine(topLeft, topRight);
        Gizmos.DrawLine(topRight, bottomRight);
        Gizmos.DrawLine(bottomRight, bottomLeft);
        Gizmos.DrawLine(bottomLeft, topLeft);
    }
}
