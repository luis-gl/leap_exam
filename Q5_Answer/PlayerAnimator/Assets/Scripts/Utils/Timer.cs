﻿using System;
using UnityEngine;

public class Timer
{
    private bool _isRunning;
    
    public float Elapsed { get; private set; }
    public float Duration { get; private set; }
    public bool Finished => !_isRunning && Elapsed >= Duration;

    public event Action OnTimerTick;
    public event Action OnTimerFinished;
    public event Action OnTimerStopped;

    public void Play(float seconds)
    {
        Duration = seconds;
        Elapsed = 0f;
        _isRunning = true;
    }

    public void Pause() =>_isRunning = false;

    public void Tick()
    {
        if (!_isRunning) return;
        
        Elapsed += Time.deltaTime;
        OnTimerTick?.Invoke();

        if (Elapsed < Duration) return;
        
        OnTimerFinished?.Invoke();
        _isRunning = false;
    }

    public void Stop()
    {
        _isRunning = false;
        OnTimerStopped?.Invoke();
    }
}
