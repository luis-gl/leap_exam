﻿using System;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    [SerializeField] private LayerMask whatIsGround;
    [SerializeField] private Vector2 boxSize = Vector2.zero;
    [SerializeField] private float boxCastDistance = 1f;

    private bool _isGrounded;

    public bool IsGrounded
    {
        get => _isGrounded;
        private set
        {
            if (value == _isGrounded) return;
            _isGrounded = value;
            OnIsGroundedChange?.Invoke(value);
        }
    }

    public event Action<bool> OnIsGroundedChange;

    private void Update()
    {
        var hit = Physics2D.BoxCast(transform.position, boxSize, 0f, Vector2.down, boxCastDistance, whatIsGround);
        IsGrounded = hit.collider != null;
    }

#if UNITY_EDITOR

    private void OnDrawGizmosSelected()
    {
        if (Application.isPlaying)
            Gizmos.color = IsGrounded ? Color.green : Color.red;
        
        GizmosUtils.DrawBoxCast(transform.position, boxSize, Vector2.down, boxCastDistance);
    }

#endif
}
