﻿using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class PlayerCombatHandler : MonoBehaviour
{
    private PlayerController _controller;

    private readonly Timer _meleeComboChainTimer = new Timer();
    private readonly Timer _rangeComboChainTimer = new Timer();

    private void Awake()
    {
        _controller = GetComponent<PlayerController>();
        if (!_controller.Initialized)
            _controller.OnInitialized += InitializeWithDependencies;
        else
            InitializeWithDependencies();
    }

    private void OnDisable() => UnregisterEvents();

    private void InitializeWithDependencies()
    {
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        if (!_controller) return;
        _controller.OnIsMeleeAttackingChange += OnIsMeleeAttackingChange;
        _controller.OnIsRangeAttackingChange += OnIsRangeAttackingChange;
        _controller.OnIsGroundedChange += OnIsGroundedChange;

        if (!_controller.Input) return;
        _controller.Input.MeleeAttackEvent += OnMeleeAttack;
        _controller.Input.RangeAttackEvent += OnRangeAttack;

        if (_meleeComboChainTimer != null)
        {
            _meleeComboChainTimer.OnTimerFinished += RestartMeleeCombo;
            _meleeComboChainTimer.OnTimerStopped += RestartMeleeCombo;
        }

        if (_rangeComboChainTimer != null)
        {
            _rangeComboChainTimer.OnTimerFinished += RestartRangeCombo;
            _rangeComboChainTimer.OnTimerStopped += RestartRangeCombo;
        }
    }

    private void UnregisterEvents()
    {
        if (!_controller) return;
        _controller.OnIsMeleeAttackingChange -= OnIsMeleeAttackingChange;
        _controller.OnIsRangeAttackingChange -= OnIsRangeAttackingChange;
        _controller.OnIsGroundedChange -= OnIsGroundedChange;

        if (!_controller.Input) return;
        _controller.Input.MeleeAttackEvent -= OnMeleeAttack;
        _controller.Input.RangeAttackEvent -= OnRangeAttack;

        if (_meleeComboChainTimer != null)
        {
            _meleeComboChainTimer.OnTimerFinished -= RestartMeleeCombo;
            _meleeComboChainTimer.OnTimerStopped -= RestartMeleeCombo;
        }

        if (_rangeComboChainTimer != null)
        {
            _rangeComboChainTimer.OnTimerFinished -= RestartRangeCombo;
            _rangeComboChainTimer.OnTimerStopped -= RestartRangeCombo;
        }
    }

    private void Update()
    {
        _meleeComboChainTimer.Tick();
        _rangeComboChainTimer.Tick();
    }

    private void OnIsMeleeAttackingChange(bool isMeleeAttacking)
    {
        if (isMeleeAttacking) return;

        _meleeComboChainTimer.Play(_controller.PlayerStats.TimeForComboChain);
    }

    private void OnIsRangeAttackingChange(bool isRangeAttacking)
    {
        if (isRangeAttacking) return;
        
        _rangeComboChainTimer.Play(_controller.PlayerStats.TimeForComboChain);
    }

    private void OnMeleeAttack()
    {
        if (_controller.IsMeleeAttacking || _controller.IsRangeAttacking) return;

        _controller.IsMeleeAttacking = true;
        _controller.IsChainingAttacks = true;
        _controller.ActiveMeleeAttackValidated();

        _controller.MeleeComboStep++;
        _controller.MeleeComboStep %= _controller.PlayerStats.MaxMeleeAttackChain;

        if (!_meleeComboChainTimer.Finished)
            _meleeComboChainTimer.Pause();
        
        if (!_rangeComboChainTimer.Finished)
            _rangeComboChainTimer.Stop();
    }

    private void OnRangeAttack()
    {
        if (_controller.IsMeleeAttacking || _controller.IsRangeAttacking
            || !_controller.IsGrounded) return;

        _controller.IsRangeAttacking = true;
        _controller.IsChainingAttacks = true;
        _controller.ActiveRangeAttackValidated();

        _controller.RangeComboStep++;
        _controller.RangeComboStep %= _controller.PlayerStats.MaxRangeAttackChain;

        if (!_rangeComboChainTimer.Finished)
            _rangeComboChainTimer.Pause();

        if (!_meleeComboChainTimer.Finished)
            _meleeComboChainTimer.Stop();
    }

    private void OnIsGroundedChange(bool isGrounded) => RestartMeleeCombo();

    private void RestartMeleeCombo()
    {
        _controller.MeleeComboStep = 0;
        _controller.IsChainingAttacks = false;
    }

    private void RestartRangeCombo()
    {
        _controller.RangeComboStep = 0;
        _controller.IsChainingAttacks = false;
    }
}
