﻿using UnityEngine;

[RequireComponent(typeof(PlayerController), typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    private float _targetSpeed;
    private float _bufferedTargetSpeed;
    private float _lastGroundTime;
    private float _startGravityScale;
    private float _jumpGravityScale;
    private float _fallGravityScale;
    private float _bufferedGravityScale;

    private PlayerController _controller;
    private Rigidbody2D _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _startGravityScale = _rigidbody.gravityScale;

        _controller = GetComponent<PlayerController>();
        if (!_controller.Initialized)
            _controller.OnInitialized += InitializeWithDependencies;
        else
            InitializeWithDependencies();
    }

    private void OnDisable() => UnregisterEvents();

    private void InitializeWithDependencies()
    {
        RegisterEvents();
        _jumpGravityScale = _startGravityScale * _controller.PlayerStats.JumpGravityModifier;
        _fallGravityScale = _startGravityScale * _controller.PlayerStats.FallGravityModifier;
    }

    private void RegisterEvents()
    {
        if (!_controller) return;
        _controller.OnMeleeAttackValidated += OnAttack;
        _controller.OnRangeAttackValidated += OnAttack;
        _controller.OnIsChainingAttacksChange += OnIsChainingAttacksChange;
        _controller.OnIsFallingChange += OnIsFallingChange;
        _controller.OnIsRollingChange += OnIsRollingChange;

        if (!_controller.Input) return;
        _controller.Input.MoveEvent += OnMove;
        _controller.Input.RollEvent += OnRoll;
        _controller.Input.JumpEvent += OnJump;
        _controller.Input.JumpCanceledEvent += OnJumpCanceled;
    }

    private void UnregisterEvents()
    {
        if (!_controller) return;
        _controller.OnInitialized -= InitializeWithDependencies;
        _controller.OnMeleeAttackValidated -= OnAttack;
        _controller.OnRangeAttackValidated -= OnAttack;
        _controller.OnIsChainingAttacksChange -= OnIsChainingAttacksChange;
        _controller.OnIsFallingChange -= OnIsFallingChange;
        _controller.OnIsRollingChange -= OnIsRollingChange;

        if (!_controller.Input) return;
        _controller.Input.MoveEvent -= OnMove;
        _controller.Input.RollEvent -= OnRoll;
        _controller.Input.JumpEvent -= OnJump;
        _controller.Input.JumpCanceledEvent -= OnJumpCanceled;
    }

    private void Update()
    {
        _lastGroundTime -= Time.deltaTime;

        if (!_controller.IsJumping || !(_rigidbody.velocity.y <= 0f)) return;
        _controller.IsJumping = false;
        _controller.IsFalling = true;
    }

    private void FixedUpdate()
    {
        #region Movement

        var speedDiff = _targetSpeed - _rigidbody.velocity.x;
        var accelRate = Mathf.Abs(_targetSpeed) > Globals.Epsilon
            ? _controller.PlayerStats.MovementAcceleration
            : _controller.PlayerStats.MovementDeceleration;
        var movement = Mathf.Pow(Mathf.Abs(speedDiff) * accelRate,
            _controller.PlayerStats.VelocityPower) * Mathf.Sign(speedDiff);
        _rigidbody.AddForce(movement * Vector2.right);

        #endregion

        #region Friction

        if (_lastGroundTime > 0f && Mathf.Abs(_targetSpeed) < Globals.Epsilon)
        {
            var amount = Mathf.Min(Mathf.Abs(_rigidbody.velocity.x), 
                Mathf.Abs(_controller.PlayerStats.Friction));
            amount *= Mathf.Sign(_rigidbody.velocity.x);
            _rigidbody.AddForce(Vector2.right * -amount, ForceMode2D.Impulse);
        }

        #endregion

        #region CoyoteTime

        if (_controller.IsGrounded) _lastGroundTime = _controller.PlayerStats.CoyoteTime;

        #endregion
    }

    private void OnMove(float direction)
    {
        _targetSpeed = direction * _controller.PlayerStats.MovementSpeed;
        _bufferedTargetSpeed = _targetSpeed;
    }

    private void OnRoll()
    {
        if (!_controller.IsGrounded || Mathf.Abs(_rigidbody.velocity.y) > Globals.Epsilon
            || _controller.IsRolling) return;

        _targetSpeed = 0;
        _controller.IsRolling = true;
        var force = _controller.LastDirection * _controller.PlayerStats.RollSpeed;
        _rigidbody.AddForce(Vector2.right * force, ForceMode2D.Impulse);
    }

    private void OnIsRollingChange(bool isRolling)
    {
        if (isRolling) return;

        _targetSpeed = _bufferedTargetSpeed;
    }

    private void OnJump()
    {
        if (!(_lastGroundTime > 0f) || _controller.IsJumping || _controller.IsRolling) return;

        var force = _controller.PlayerStats.JumpForce;
        if (_rigidbody.velocity.y < 0f)
            force -= _rigidbody.velocity.y;
        _rigidbody.AddForce(Vector2.up * force, ForceMode2D.Impulse);

        _lastGroundTime = 0f;
        _controller.IsJumping = true;
        _controller.IsFalling = false;

        _rigidbody.gravityScale = _jumpGravityScale;
        _bufferedGravityScale = _jumpGravityScale;
    }

    private void OnJumpCanceled()
    {
        if (!(_rigidbody.velocity.y > 0f) || !_controller.IsJumping) return;

        var force = Vector2.down * _rigidbody.velocity.y * _controller.PlayerStats.JumpCutMultiplier;
        _rigidbody.AddForce(force, ForceMode2D.Impulse);
    }

    private void OnIsFallingChange(bool isFalling)
    {
        if (!isFalling) return;
        
        if (isFalling && !_controller.IsGrounded)
            _rigidbody.gravityScale = _fallGravityScale;
        else if (isFalling && _controller.IsGrounded)
            _rigidbody.gravityScale = _startGravityScale;
    }

    private void OnAttack()
    {
        if (_controller.IsRolling) return;

        _targetSpeed = 0;

        if (_controller.IsGrounded) return;

        var force = _rigidbody.velocity.y;

        if (_rigidbody.velocity.y > 0f)
        {
            _bufferedGravityScale = _jumpGravityScale;
            force -= _controller.PlayerStats.ImpulseOnAttackAscending;
        }
        else if (_rigidbody.velocity.y < 0f)
        {
            _bufferedGravityScale = _fallGravityScale;
        }

        _rigidbody.AddForce(Vector2.up * -force, ForceMode2D.Impulse);

        _rigidbody.gravityScale = 0f;
    }

    private void OnIsChainingAttacksChange(bool isChainingAttacks)
    {
        if (isChainingAttacks || _controller.IsRolling) return;

        _targetSpeed = _bufferedTargetSpeed;

        if (_controller.IsGrounded) return;
        _rigidbody.gravityScale = _bufferedGravityScale;
    }
}
