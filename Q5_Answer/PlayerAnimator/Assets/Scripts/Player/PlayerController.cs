﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Character Properties")]
    [SerializeField] private Stats playerStats;

    [Header("Physics/Scene Helpers")]
    [SerializeField] private GroundCheck groundCheck;

    [Header("Initial Configuration")]
    [SerializeField] private bool isFacingRight = true;

    private bool _isJumping;
    private bool _isFalling;
    private bool _isMeleeAttacking;
    private bool _isRangeAttacking;
    private bool _isChainingAttacks;
    private bool _isRolling;

    public PlayerInputHandler Input { get; private set; }
    public Stats PlayerStats => playerStats;
    public bool IsGrounded => groundCheck.IsGrounded;
    public bool Initialized { get; private set; }
    public float LastDirection { get; private set; }
    public int MeleeComboStep { get; set; }
    public int RangeComboStep { get; set; }
    public bool IsFacingRight
    {
        get => isFacingRight;
        set
        {
            if (isFacingRight == value) return;
            isFacingRight = value;
            LastDirection = value ? 1f : -1f;
        }
    }
    public bool IsJumping
    {
        get => _isJumping;
        set
        {
            if (_isJumping == value) return;
            _isJumping = value;
            OnIsJumpingChange?.Invoke(value);
        }
    }
    public bool IsFalling
    {
        get => _isFalling;
        set
        {
            if (_isFalling == value) return;
            _isFalling = value;
            OnIsFallingChange?.Invoke(value);
        }
    }
    public bool IsMeleeAttacking
    {
        get => _isMeleeAttacking;
        set
        {
            if (_isMeleeAttacking == value) return;
            _isMeleeAttacking = value;
            OnIsMeleeAttackingChange?.Invoke(value);
        }
    }
    public bool IsRangeAttacking
    {
        get => _isRangeAttacking;
        set
        {
            if (_isRangeAttacking == value) return;
            _isRangeAttacking = value;
            OnIsRangeAttackingChange?.Invoke(value);
        }
    }
    public bool IsChainingAttacks
    {
        get => _isChainingAttacks;
        set
        {
            if (_isChainingAttacks == value) return;
            _isChainingAttacks = value;
            OnIsChainingAttacksChange?.Invoke(value);
        }
    }
    public bool IsRolling
    {
        get => _isRolling;
        set
        {
            if (_isRolling == value) return;
            _isRolling = value;
            OnIsRollingChange?.Invoke(value);
        }
    }

    public event Action OnInitialized;
    public event Action<bool> OnIsGroundedChange;
    public event Action<bool> OnIsJumpingChange;
    public event Action<bool> OnIsFallingChange;
    public event Action<bool> OnIsMeleeAttackingChange;
    public event Action<bool> OnIsRangeAttackingChange;
    public event Action<bool> OnIsChainingAttacksChange;
    public event Action<bool> OnIsRollingChange;
    public event Action OnMeleeAttackValidated;
    public event Action OnRangeAttackValidated;

    private void Awake()
    {
        Initialized = false;

        Input = GetComponent<PlayerInputHandler>();
        RegisterEvents();

        IsJumping = false;
        IsFalling = true;

        MeleeComboStep = 0;
        RangeComboStep = 0;
        IsMeleeAttacking = false;
        IsRangeAttacking = false;
        IsChainingAttacks = false;

        LastDirection = isFacingRight ? 1f : -1f;

        OnInitialized?.Invoke();
        Initialized = true;
    }

    private void OnDisable() => UnregisterEvents();

    private void RegisterEvents()
    {
        if (!groundCheck) return;
        groundCheck.OnIsGroundedChange += ActiveIsGroundedEvent;
    }

    private void UnregisterEvents()
    {
        if (!groundCheck) return;
        groundCheck.OnIsGroundedChange -= ActiveIsGroundedEvent;
    }

    private void ActiveIsGroundedEvent(bool isGrounded) => OnIsGroundedChange?.Invoke(isGrounded);
    public void ActiveMeleeAttackValidated() => OnMeleeAttackValidated?.Invoke();
    public void ActiveRangeAttackValidated() => OnRangeAttackValidated?.Invoke();
}
