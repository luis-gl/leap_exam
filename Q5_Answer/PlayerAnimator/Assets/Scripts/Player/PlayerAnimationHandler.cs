﻿using UnityEngine;

[RequireComponent(typeof(PlayerController), typeof(Animator))]
public class PlayerAnimationHandler : MonoBehaviour
{
    private Animator _animator;
    private PlayerController _controller;

    private readonly int _horizontalVelocityAnimParam = Animator.StringToHash("HorizontalVelocity");
    private readonly int _isJumpingAnimParam = Animator.StringToHash("IsJumping");
    private readonly int _isFallingAnimParam = Animator.StringToHash("IsFalling");
    private readonly int _isGroundedAnimParam = Animator.StringToHash("IsGrounded");
    private readonly int _meleeAttackAnimParam = Animator.StringToHash("MeleeAttack");
    private readonly int _meleeComboAnimParam = Animator.StringToHash("MeleeCombo");
    private readonly int _rangeAttackAnimParam = Animator.StringToHash("RangeAttack");
    private readonly int _rangeComboAnimParam = Animator.StringToHash("RangeCombo");
    private readonly int _rollAnimParam = Animator.StringToHash("Roll");

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _controller = GetComponent<PlayerController>();
        if (!_controller.Initialized)
            _controller.OnInitialized += InitializeWithDependencies;
        else
            InitializeWithDependencies();
    }

    private void OnDisable() => UnregisterEvents();

    private void InitializeWithDependencies()
    {
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        if (!_controller) return;
        _controller.OnIsGroundedChange += OnIsGroundedChange;
        _controller.OnIsJumpingChange += OnIsJumpingChange;
        _controller.OnIsFallingChange += OnIsFallingChange;
        _controller.OnMeleeAttackValidated += OnMeleeAttack;
        _controller.OnRangeAttackValidated += OnRangeAttack;

        if (!_controller.Input) return;
        _controller.Input.MoveEvent += OnMove;
        _controller.Input.RollEvent += OnRoll;
    }

    private void UnregisterEvents()
    {
        if (!_controller) return;
        _controller.OnInitialized -= InitializeWithDependencies;
        _controller.OnIsGroundedChange -= OnIsGroundedChange;
        _controller.OnIsJumpingChange -= OnIsJumpingChange;
        _controller.OnIsFallingChange -= OnIsFallingChange;
        _controller.OnMeleeAttackValidated -= OnMeleeAttack;
        _controller.OnRangeAttackValidated -= OnRangeAttack;

        if (!_controller.Input) return;
        _controller.Input.MoveEvent -= OnMove;
        _controller.Input.RollEvent -= OnRoll;
    }

    private void Flip()
    {
        _controller.IsFacingRight = !_controller.IsFacingRight;

        var currentScale = transform.localScale;
        currentScale.x *= -1f;
        transform.localScale = currentScale;
    }

    private void OnMove(float direction)
    {
        if ((direction > Globals.Epsilon && !_controller.IsFacingRight) ||
            (direction < -Globals.Epsilon && _controller.IsFacingRight))
            Flip();

        _animator.SetInteger(_horizontalVelocityAnimParam, (int) Mathf.Abs(direction));
    }

    private void OnRoll()
    {
        if (!_controller.IsGrounded || _controller.IsRolling) return;
        _animator.SetTrigger(_rollAnimParam);
    }

    private void OnIsGroundedChange(bool isGrounded)
    {
        _animator.SetBool(_isGroundedAnimParam, isGrounded);
    }

    private void OnIsFallingChange(bool isFalling)
    {
        _animator.SetBool(_isFallingAnimParam, isFalling);
    }

    private void OnIsJumpingChange(bool isJumping)
    {
        _animator.SetBool(_isJumpingAnimParam, isJumping);
    }

    private void OnMeleeAttack()
    {
        _animator.SetTrigger(_meleeAttackAnimParam);
        _animator.SetInteger(_meleeComboAnimParam, _controller.MeleeComboStep);
    }

    private void OnRangeAttack()
    {
        _animator.SetTrigger(_rangeAttackAnimParam);
        _animator.SetInteger(_rangeComboAnimParam, _controller.RangeComboStep);
    }
}
