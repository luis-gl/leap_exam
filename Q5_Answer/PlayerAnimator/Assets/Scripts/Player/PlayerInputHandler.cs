﻿using System;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class PlayerInputHandler : MonoBehaviour
{
    public event Action<float> MoveEvent;
    public event Action JumpEvent;
    public event Action JumpCanceledEvent;
    public event Action MeleeAttackEvent;
    public event Action RangeAttackEvent;
    public event Action RollEvent;

    private void Update()
    {
        // Trigger move event only if the button was pressed or released
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D)
            || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            MoveEvent?.Invoke(Input.GetAxisRaw("Horizontal"));
        }

        if (Input.GetKeyDown(KeyCode.Space))
            JumpEvent?.Invoke();
        if (Input.GetKeyUp(KeyCode.Space))
            JumpCanceledEvent?.Invoke();

        if (Input.GetKeyDown(KeyCode.J))
            MeleeAttackEvent?.Invoke();
        
        if (Input.GetKeyDown(KeyCode.I))
            RangeAttackEvent?.Invoke();
        
        if (Input.GetKeyDown(KeyCode.L))
            RollEvent?.Invoke();
    }
}
