﻿using UnityEngine;

[CreateAssetMenu(fileName = "Stats", menuName = "Scriptable Objects/Stats")]
public class Stats : ScriptableObject
{
    [SerializeField] private float timeForComboChain = 0.2f;
    [SerializeField] private int maxMeleeAttackChain = 3;
    [SerializeField] private int maxRangeAttackChain = 3;
    [SerializeField] private float movementSpeed = 5f;
    [SerializeField] private float movementAcceleration = 3f;
    [SerializeField] private float movementDeceleration = 3f;
    [SerializeField] private float friction = 0.2f;
    [SerializeField] private float velocityPower = 0.9f;
    [SerializeField] private float jumpForce = 3f;
    [SerializeField] private float jumpCutMultiplier = 0.5f;
    [SerializeField] private float impulseOnAttackAscending = 0.01f;
    [SerializeField] private float coyoteTime = 0.1f;
    [SerializeField] private float jumpGravityModifier = 1f;
    [SerializeField] private float fallGravityModifier = 3f;
    [SerializeField] private float rollSpeed = 5f;

    public float TimeForComboChain => timeForComboChain;
    public int MaxMeleeAttackChain => maxMeleeAttackChain;
    public int MaxRangeAttackChain => maxRangeAttackChain;
    public float MovementSpeed => movementSpeed;
    public float MovementAcceleration => movementAcceleration;
    public float MovementDeceleration => movementDeceleration;
    public float Friction => friction;
    public float VelocityPower => velocityPower;
    public float JumpForce => jumpForce;
    public float JumpCutMultiplier => 1f - jumpCutMultiplier;
    public float ImpulseOnAttackAscending => impulseOnAttackAscending;
    public float CoyoteTime => coyoteTime;
    public float JumpGravityModifier => jumpGravityModifier;
    public float FallGravityModifier => fallGravityModifier;
    public float RollSpeed => rollSpeed;
}
