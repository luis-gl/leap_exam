﻿using UnityEngine;

[CreateAssetMenu(fileName = "Game Configuration", menuName = "ScriptableObjects/Config/Game Configuration")]
public class GameConfiguration : ScriptableObject
{
    [Header("General Configuration")]
    [SerializeField] private int expectedTubesQuantity = 4;

    [Header("Tube Detection Configuration")]
    [SerializeField] private LayerMask tubeLayerMask;
    [SerializeField] private float detectionDistance = 1f;
    [SerializeField] private float detectionEpsilon = 0.00001f;

    public int ExpectedTubesQuantity => expectedTubesQuantity;
    public LayerMask TubeLayerMask => tubeLayerMask;
    public float DetectionDistance => detectionDistance;
    public float DetectionEpsilon => detectionEpsilon;
}
