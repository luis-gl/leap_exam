﻿using UnityEngine;
using UnityEngine.UI;

namespace GameUI
{
    public class GameUI : MonoBehaviour
    {
        private Text _finishText;

        private void OnEnable()
        {
            if (GameManager.Instance)
                Initialize();
            else
                GameManager.OnInstanceCreated += Initialize;
        }

        private void OnDisable()
        {
            Unregister();
        }

        private void Initialize()
        {
            if (!_finishText)
                _finishText = GetComponent<Text>();

            _finishText.enabled = false;
            GameManager.Instance.OnGameFinished += ActiveWinText;
        }

        private void Unregister()
        {
            GameManager.OnInstanceCreated -= Initialize;

            if (!GameManager.Instance) return;
            GameManager.Instance.OnGameFinished -= ActiveWinText;
        }

        private void ActiveWinText()
        {
            _finishText.enabled = true;
        }
    }
}