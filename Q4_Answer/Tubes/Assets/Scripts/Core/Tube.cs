﻿using System;
using UnityEngine;

namespace Core
{
    [RequireComponent(typeof(BoxCollider2D), typeof(SpriteRenderer))]
    public class Tube : MonoBehaviour
    {
        [SerializeField] private Connection[] connectionSides;

        private Tube[] _allSideTubes;
        private Connection[] _allSideConnections;
        private bool[] _allSideExistence;

        private const float rotationAngle = 90f;

        public bool AllSidesConnected { get; private set; }

        public event Action OnAllSidesConnected;

        private void OnEnable()
        {
            if (GameManager.Instance)
                Initialize();
            else
                GameManager.OnInstanceCreated += Initialize;
        }

        private void OnDisable()
        {
            GameManager.OnInstanceCreated -= Initialize;

            if (!GameManager.Instance) return;
            GameManager.Instance.OnGameStarted -= CheckAtStart;
            GameManager.Instance.RemoveTube(this);
        }

        private void Initialize()
        {
            CreateAllSideTubesArray();
            CreateAllSideConnectionsArray();
            CreateSideExistencialArray();
            InitializeConnections();
            GameManager.Instance.OnGameStarted += CheckAtStart;
            GameManager.Instance.RegisterTube(this);
        }

        private void CreateAllSideTubesArray()
        {
            var bounds = GetComponent<Collider2D>().bounds;

            var sideValues = Enum.GetValues(typeof(Side));
            _allSideTubes = new Tube[sideValues.Length];

            var center = (Vector2) bounds.center;
            var castDistance = GameManager.Instance.Configuration.DetectionDistance;
            var detectionEpsilon = GameManager.Instance.Configuration.DetectionEpsilon;
            var layerMask = GameManager.Instance.Configuration.TubeLayerMask;

            foreach (Side side in sideValues)
            {
                var castDirection = GetCastDirection(side);
                var position = center + castDirection * (GetExtentFromSide(side, bounds.extents) + detectionEpsilon);
                var hit = Physics2D.Raycast(position, castDirection, castDistance, layerMask);
                if (hit.collider)
                    _allSideTubes[(int) side] = hit.collider.GetComponent<Tube>();
            }
        }

        private float GetExtentFromSide(Side side, Vector2 extents)
        {
            if (side == Side.Up || side == Side.Down)
                return extents.y;
            
            return extents.x;
        }

        private Vector2 GetCastDirection(Side side)
        {
            switch (side)
            {
                case Side.Up:
                    return Vector2.up;
                case Side.Down:
                    return Vector2.down;
                case Side.Left:
                    return Vector2.left;
                case Side.Right:
                    return Vector2.right;
                default:
                    return Vector2.up;
            }
        }

        private void CreateSideExistencialArray()
        {
            _allSideExistence = new bool[_allSideTubes.Length];
            UpdateSideExistencialArray(true);
        }

        private void UpdateSideExistencialArray(bool value)
        {
            foreach (var connection in connectionSides)
            {
                _allSideExistence[(int) connection.ConnectionSide] = value;
            }
        }

        private void CreateAllSideConnectionsArray()
        {
            _allSideConnections = new Connection[_allSideTubes.Length];
            UpdateAllSideConnectionsArray(true);
        }

        private void UpdateAllSideConnectionsArray(bool assign)
        {
            foreach (var connection in connectionSides)
            {
                var index = (int) connection.ConnectionSide;
                _allSideConnections[index] = assign ? connection : null;
            }
        }

        private void InitializeConnections()
        {
            foreach (var connection in connectionSides)
            {
                connection.Initialize(this);
            }
        }

        private void OnMouseDown()
        {
            UpdateSideExistencialArray(false);
            UpdateAllSideConnectionsArray(false);
            Rotate();
            UpdateSideExistencialArray(true);
            UpdateAllSideConnectionsArray(true);
            CheckAllSidesConnected();
        }

        [ContextMenu("Rotate 90° Degrees Clockwise")]
        public void Rotate()
        {
            transform.Rotate(Vector3.back, rotationAngle);
            foreach (var connection in connectionSides)
            {
                connection.NextConnectionSide();
                connection.CheckConnection();
            }
        }

        private void CheckAtStart()
        {
            CheckSideConnections();
            CheckAllSidesConnected();
        }

        private void CheckSideConnections()
        {
            foreach (var connection in connectionSides)
            {
                connection.CheckConnection();
            }
        }

        private void CheckAllSidesConnected()
        {
            foreach (var connection in connectionSides)
            {
                if (!connection.IsConnected)
                {
                    AllSidesConnected = false;
                    return;
                }
            }
            
            AllSidesConnected = true;
            OnAllSidesConnected?.Invoke();
        }

        public void UpdateSideConnection(Side side, string caller)
        {
            _allSideConnections[(int) side].IsConnected = true;
            CheckAllSidesConnected();
        }

        public Tube GetTubeAtSide(Side side)
        {
            return _allSideTubes[(int) side];
        }

        public bool HasTubeAtSide(Side side)
        {
            return _allSideTubes[(int) side] != null;
        }

        public bool HasConnectionAtSide(Side side)
        {
            return _allSideExistence[(int) side];
        }
    }
}