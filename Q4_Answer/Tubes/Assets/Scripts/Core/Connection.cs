﻿using UnityEngine;

namespace Core
{
    public enum Side
    {
        Up = 0,
        Down = 1,
        Left = 2,
        Right = 3
    }

    [System.Serializable]
    public class Connection
    {
        [SerializeField] private Side connectionSide;

        private Tube _owner;
        private Side _oppositeSide;

        public bool IsConnected { get; set; }
        public Side ConnectionSide => connectionSide;

        public void Initialize(Tube tube)
        {
            _owner = tube;
            _oppositeSide = GetOpposite();
        }

        public void NextConnectionSide()
        {
            connectionSide = GetNextSide();
            _oppositeSide = GetOpposite();
        }

        public void CheckConnection()
        {
            if (!Application.isPlaying) return;

            if (!_owner.HasTubeAtSide(connectionSide))
            {
                IsConnected = false;
                return;
            }

            var tube = _owner.GetTubeAtSide(connectionSide);
            IsConnected = tube.HasConnectionAtSide(_oppositeSide);

            if (!IsConnected) return;

            tube.UpdateSideConnection(_oppositeSide, _owner.name);
        }

        private Side GetOpposite()
        {
            switch (connectionSide)
            {
                case Side.Up:
                    return Side.Down;
                case Side.Down:
                    return Side.Up;
                case Side.Left:
                    return Side.Right;
                case Side.Right:
                    return Side.Left;
                default:
                    return Side.Down;
            }
        }

        private Side GetNextSide()
        {
            switch(connectionSide)
            {
                case Side.Up:
                    return Side.Right;
                case Side.Down:
                    return Side.Left;
                case Side.Left:
                    return Side.Up;
                case Side.Right:
                    return Side.Down;
                default:
                    return Side.Right;
            }
        }
    }
}