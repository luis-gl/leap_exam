﻿using Core;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameConfiguration configuration;

    private List<Tube> _tubes = new List<Tube>();

    public static GameManager Instance { get; private set; }
    public GameConfiguration Configuration => configuration;

    public static event Action OnInstanceCreated;
    public event Action OnGameStarted;
    public event Action OnGameFinished;

    private void Awake()
    {
        MakeSingleton();
    }

    private void MakeSingleton()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);

        Instance = this;
        OnInstanceCreated?.Invoke();
    }

    private void CheckGameFinished()
    {
        foreach (var tube in _tubes)
        {
            if (!tube.AllSidesConnected) return;
        }
        print("Game Finish");
        OnGameFinished?.Invoke();
    }

    public void RegisterTube(Tube newTube)
    {
        if (_tubes.Contains(newTube)) return;

        _tubes.Add(newTube);
        newTube.OnAllSidesConnected += CheckGameFinished;

        if (_tubes.Count == configuration.ExpectedTubesQuantity)
        {
            print("Game Started");
            OnGameStarted?.Invoke();
        }
    }

    public void RemoveTube(Tube newTube)
    {
        if (!_tubes.Contains(newTube)) return;

        _tubes.Remove(newTube);
        newTube.OnAllSidesConnected -= CheckGameFinished;
    }
}
