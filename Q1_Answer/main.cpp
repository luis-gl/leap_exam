#include <iostream>
#include <math.h>

bool xyz(int n)
{
    float i = sqrt(n);
    int j = ceil(i);
    int k = 2;
    int x = k;
    while (x <= j)
    {
        if (!(n % x))
            return false;
        else
            x++;
    }
    return true;
}

int main()
{
    std::string values[2] = {"false", "true"};      // 0 = false, 1 = true
    for (size_t i = 0; i < 51; i++)
    {
        std::cout << i << ": " << values[xyz(i)] << "\n";
    }
    return 0;
}