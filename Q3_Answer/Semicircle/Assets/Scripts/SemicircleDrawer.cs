﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class SemicircleDrawer : MonoBehaviour
{
    [Header("Drawing Settings")]
    [SerializeField] private int drawSteps = 360;
    [SerializeField] private float lineWidth = 0.1f;
    [SerializeField] private float pointSize = 0.1f;

    [Header("Algorithm Input")]
    [SerializeField] private Vector2 center = Vector2.zero;
    [SerializeField] private float radius = 1f;
    [SerializeField] private int pointQuantity = 5;

    private LineRenderer _lineRenderer;
    private GameObject _pointPrefab;

    private const float semicircleAngle = 180f;

    private void Awake()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.startWidth = lineWidth;

        _pointPrefab = Resources.Load<GameObject>("Prefabs/Point");
    }

    private void Start()
    {
        DrawSemicircle();
        DrawPoints();
    }

    private void DrawSemicircle()
    {
        _lineRenderer.positionCount = drawSteps + 1;

        for (var i = 0; i <= drawSteps; i++)
        {
            var angle = semicircleAngle * i / drawSteps;
            var position = GetCircunferencePoint(angle) + center;
            _lineRenderer.SetPosition(i, position);
        }
    }

    private void DrawPoints()
    {
        for (var i = 0; i < pointQuantity; i++)
        {
            var angle = Random.Range(0f, 180f);
            var position = GetCircunferencePoint(angle) + center;
            var point = Instantiate(_pointPrefab, position, Quaternion.identity);
            point.transform.localScale = Vector2.one * pointSize;

            print($"Angle: {angle} - Point: ({position.x}, {position.y})");
        }
    }

    private Vector2 GetCircunferencePoint(float angle)
    {
        var radians = angle * Mathf.Deg2Rad;
        var x = radius * Mathf.Cos(radians);
        var y = radius * Mathf.Sin(radians);

        return new Vector2(x, y);
    }
}
