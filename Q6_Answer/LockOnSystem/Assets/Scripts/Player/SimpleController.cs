﻿using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(CharacterController))]
    public class SimpleController : MonoBehaviour
    {
        [SerializeField] private Transform cameraTransform;
        [SerializeField] private float speed = 5f;

        private float _horizontal;
        private float _forward;

        private Vector3 _directionVector;
        private Vector3 _cameraForward;
        private Vector3 _cameraRight;
        private CharacterController _character;

        public Transform CameraTransform => cameraTransform;

        private void Awake()
        {
            _character = GetComponent<CharacterController>();
        }

        private void Update()
        {
            _horizontal = Input.GetAxisRaw("Horizontal");
            _forward = Input.GetAxisRaw("Vertical");

            _cameraForward = cameraTransform.forward;
            _cameraRight = cameraTransform.right;

            _cameraForward.y = 0f;
            _cameraRight.y = 0f;
            
            _cameraForward = _cameraForward.normalized;
            _cameraRight = _cameraRight.normalized;

            _directionVector = _cameraForward * _forward + _cameraRight * _horizontal;
            _directionVector = _directionVector.normalized;
        }

        private void FixedUpdate()
        {
            transform.LookAt(transform.position + _directionVector);

            if (_directionVector.magnitude < 0.1f) return;

            _character.Move(_directionVector * speed * Time.deltaTime);
        }
    }
}