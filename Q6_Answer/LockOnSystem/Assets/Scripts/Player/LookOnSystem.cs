﻿using Enemy;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class LookOnSystem : MonoBehaviour
    {
        [SerializeField] private KeyCode activeLockOnKey = KeyCode.Q;
        [SerializeField] private float maxEffectiveDistance = 20f;
        // Angle that the system will take for left and right sides from forward vector
        [SerializeField] private float priorityMaxAngle = 45f;
        [SerializeField] private bool useCameraForward = true;

        private SimpleController _controller;

        private List<EnemyEntity> _priorityEnemies = new List<EnemyEntity>();
        private List<EnemyEntity> _forwardEnemies = new List<EnemyEntity>();
        private List<EnemyEntity> _backwardEnemies = new List<EnemyEntity>();

        private void Awake()
        {
            _controller = GetComponent<SimpleController>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(activeLockOnKey))
            {
                var currentForward = useCameraForward ? _controller.CameraTransform.forward : transform.forward;
                currentForward.y = 0f;
                
                var target = LockOnEnemy(transform.position, currentForward, GameManager.Enemies);
                print($"Selected {target.name}");
                transform.LookAt(target.Position);
            }
        }

        public EnemyEntity LockOnEnemy(Vector3 position, Vector3 forward, List<EnemyEntity> enemies)
        {
            CheckForClearList(_priorityEnemies);
            CheckForClearList(_forwardEnemies);
            CheckForClearList(_backwardEnemies);

            foreach (var enemy in enemies)
            {
                if (!enemy.IsRendered) continue;

                var distanceFromPlayer = Vector3.Distance(enemy.Position, position);

                if (distanceFromPlayer > maxEffectiveDistance) continue;

                enemy.DistanceFromPlayer = distanceFromPlayer;

                var directionFromPlayer = (enemy.Position - position).normalized;
                var angleFromForward = Vector3.Angle(forward, directionFromPlayer);
                
                // angleFromForward is in range [0, 180] so the angle setted in inspector is for both sides.
                // The total angle of the cone of vision for lock on is 2 * priorityMaxAngle.
                if (angleFromForward <= priorityMaxAngle)
                    _priorityEnemies.Add(enemy);
                else if (angleFromForward <= 90f)
                    _forwardEnemies.Add(enemy);
                else
                    _backwardEnemies.Add(enemy);
            }

            var target = CheckListCondition(_priorityEnemies);
            if (target != null) return target;

            target = CheckListCondition(_forwardEnemies);
            if (target != null) return target;

            target = CheckListCondition(_backwardEnemies);
            return target;
        }

        private void CheckForClearList(List<EnemyEntity> list)
        {
            if (list.Count == 0) return;

            list.Clear();
        }

        private EnemyEntity CheckListCondition(List<EnemyEntity> list)
        {
            if (list.Count == 0) return null;
            if (list.Count == 1) return list[0];

            list.Sort();
            return list[0];
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            var position = transform.position;
            var coordinateTransform = useCameraForward ?
                GetComponent<SimpleController>().CameraTransform
                : transform;
            var right = coordinateTransform.right;
            right.y = 0f;
            right = right.normalized;
            var forward = coordinateTransform.forward;
            forward.y = 0f;
            forward = forward.normalized;

            var leftLimit = GetCircunferencePoint(priorityMaxAngle) * maxEffectiveDistance;
            var rightLimit = leftLimit;
            rightLimit.x *= -1f;

            leftLimit = RotateVectorInSelfSpace(leftLimit, right, forward) + position;
            rightLimit = RotateVectorInSelfSpace(rightLimit, right, forward) + position;

            forward *= maxEffectiveDistance;
            forward += position;

            Gizmos.DrawLine(position, leftLimit);
            Gizmos.DrawLine(position, rightLimit);
            Gizmos.DrawLine(leftLimit, forward);
            Gizmos.DrawLine(rightLimit, forward);
        }

        private Vector3 GetCircunferencePoint(float angle)
        {
            var radians = (angle + 90f) * Mathf.Deg2Rad;
            var x = Mathf.Cos(radians);
            var z = Mathf.Sin(radians);

            return new Vector3(x, 0f, z);
        }

        private Vector3 RotateVectorInSelfSpace(Vector3 v, Vector3 right, Vector3 forward)
        {
            return v.x * right + v.z * forward;
        }
    }
#endif
}