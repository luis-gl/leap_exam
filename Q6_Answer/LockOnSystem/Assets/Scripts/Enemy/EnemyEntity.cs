﻿using System;
using UnityEngine;

namespace Enemy
{
    public class EnemyEntity : MonoBehaviour, IComparable<EnemyEntity>
    {
        [SerializeField] private bool isRendered;

        public Vector3 Position => transform.position;
        public bool IsRendered => isRendered;
        public float DistanceFromPlayer { get; set; }

        private void OnBecameInvisible() => isRendered = false;
        private void OnBecameVisible() => isRendered = true;

        private void OnEnable() => GameManager.RegisterEnemy(this);
        private void OnDisable() => GameManager.UnregisterEnemy(this);

        public int CompareTo(EnemyEntity other)
        {
            if (other == null) return 1;

            return DistanceFromPlayer.CompareTo(other.DistanceFromPlayer);
        }

        public static bool operator > (EnemyEntity comp1, EnemyEntity comp2)
        {
            return comp1.CompareTo(comp2) > 0;
        }

        public static bool operator < (EnemyEntity comp1, EnemyEntity comp2)
        {
            return comp1.CompareTo(comp2) < 0;
        }

        public static bool operator >= (EnemyEntity comp1, EnemyEntity comp2)
        {
            return comp1.CompareTo(comp2) >= 0;
        }

        public static bool operator <= (EnemyEntity comp1, EnemyEntity comp2)
        {
            return comp1.CompareTo(comp2) <= 0;
        }
    }
}