﻿using UnityEngine;

namespace Camera
{
    public class SimpleThirdPersonCamera : MonoBehaviour
    {
        [Header("Look Config")]
        [SerializeField] private Transform target;

        [Header("Movement Config")]
        [SerializeField] private Vector3 offset;
        [SerializeField] private float sensibility;
        [SerializeField, Range(0f, 1f)] private float smoothPositionValue;

        private void FixedUpdate()
        {
            transform.position = Vector3.Lerp(transform.position, target.position + offset, smoothPositionValue);
            offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * sensibility, Vector3.up) * offset;

            transform.LookAt(target);
        }
    }
}