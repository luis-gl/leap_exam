﻿using Enemy;
using Player;
using System.Collections.Generic;

public static class GameManager
{
    public static readonly List<EnemyEntity> Enemies = new List<EnemyEntity>();

    public static void RegisterEnemy(EnemyEntity member)
    {
        if (Enemies.Contains(member)) return;

        Enemies.Add(member);
    }

    public static void UnregisterEnemy(EnemyEntity member)
    {
        if (!Enemies.Contains(member)) return;

        Enemies.Remove(member);
    }
}