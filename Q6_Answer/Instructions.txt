Versión de Unity usada: 2019.4.32f1

- En un proyecto Unity importar el paquete que se encuentra en la carpeta "Q6_Answer/Package/LockOnSystem.unitypackage".
- Caso contrario abrir el proyecto que se encuentra en la carpeta "Q6_Answer/LockOnSystem".
- Ir a la escena "LockOnSystem" y en el objeto en escena "Player" ir al componente "Lock On System", ahí se pueden modificar las siguientes propiedades:
    * Active Lock On Key: tipo KeyCode, define que tecla usar para activar el algoritmo.
    * Max Effective Distance: tipo float, define la distancia efectiva máxima a la que funciona el algoritmo.
    * Priority Max Angle: tipo float, define el ángulo máximo al que los objetivos prioritarios deben encontrarse para ser seleccionados a cada lado del vector que define la vista frontal.
    * Use Camera Forward: tipo bool, define si usar la visión de la cámara en vez de la del personaje para el funcionamiento del algoritmo.
- Ejecutar el juego:
    * Se puede mover con WASD.
    * La cámara se mueve con el mouse.
    * Activa la selección de objetivo con la tecla definida en el componente.
    * Si encuentra un objetivo, el personaje volteará en su dirección y se imprimirá un mensaje en la consola.