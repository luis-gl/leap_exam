El sistema se basa en archivos locales y en la nube y trabaja bajo estas condiciones.

- En primera instancia, cuando el usuario entra al juego debe tener una cuenta de usuario o crearla por primera vez, de esta manera queda
  registrado en la base de datos en la nube.
- Dependiendo del tipo de currency (si es utilizable comúnmente en el juego o si es adquirida con dinero real), se debe permanecer conectado
  a internet (loggeado).

Registro de transacciones:
- Las transacciones se guardan en un archivo persistente requerido por el juego ya sea que se encuentre online u offline.
- En caso de encontrarse offline, se genera un archivo temporal que guarda las transacciones realizadas en este modo (además de guardarlas localmente)
  este se borarrá cuando sea verificado al conectarse a internet.

Restauración de transacciones:
- Al entrar al juego, la aplicación se loggea en la base de datos en la nube.
- Se solicita la revisión del archivo temporal de transacciones offline y se guarda las entradas en la nube.
- Un vez terminada la revisión, se elimina el archivo temporal y se verifica la integridad del historial de transacciones local y se restaura en caso
  de ser necesario.